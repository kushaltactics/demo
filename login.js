var LocalStrategy   = require('passport-local').Strategy;
var User = require('./user');
//var bCrypt = require('bcrypt-nodejs');

module.exports = function(passport){

	passport.use('local', new LocalStrategy({
            passReqToCallback : true
        },
        function(req, username, password, done) { 
        	
        	console.log("passport local funct"+username+"req"+JSON.stringify(req.query));
        	var userAttributes=req.query;
    		userAttributes.email=username;
    		userAttributes.password=password;
    		    		
    		User.populateUser(userAttributes);
    		User.print();
    		User.getUser(function(err,user){
    		
    		console.log('response of login request:'+JSON.stringify(user));
            // In case of any error, return using the done method
            if (err)
                return done(err);
            // Username does not exist, log the error and redirect back
            if (!user){
                console.log('User Not Found with username '+username);
                
                return done(null, false,{ message: 'bad password' });                 
            }
            // User and password both match, return user from done method
            // which will be treated like success
           	if(bcrypt.compareSync(password, user.password)){
           		return done(null, user);
           	}
           	else{
           		console.log('Password Hash does not match for '+password);
           		return done(null, false,{ message: 'Bad Password' }); 
           	}

    		});

        })
    );
    
}

