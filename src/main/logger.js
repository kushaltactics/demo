/**
 * This file creates a logger instance that will be used by all other files
 */

var winston = require('winston');

var winston = new (winston.Logger)({
    transports: [
        // for production change level to 'info' instead of debug
        new (winston.transports.Console)({ level: 'info' }),
        new (winston.transports.File)({ filename: __dirname + '../logs/appLogs.log', level: 'debug' })
    ]
});

winston.info('Logger Initialised" ')

module.exports = winston;