var db = require('./dbConnectionModule.js');
var mongo = require('mongoskin');
var BSON = mongo.BSONPure;
module.exports = {
	dashboardBorrower : function(req, res) {
		var userId = new BSON.ObjectID(req.query.userId);
		var loanRequests = [];
		var responseBody = {};

		/*
		 * To insert data in database db.collection('loanRequest').insert({
		 * "borrowerId": "vishal@edgeverve.com", "purpose": "Real Estate",
		 * "totalAmount": "20000", "interestRate": "15", "term": "12",
		 * "emiAmount": "1917", "pendingAmount": "23000", "createdDate": Date(),
		 * "activeDate": Date(), "status": "active" },function(err,doc){
		 * console.log('loan request added successfully.' + err+ doc); });
		 */
		console.log('Borrower Dashboard starts');
		db
				.collection("loanRequest")
				.find({
					'borrowerId' : userId
				})
				.toArray(
						function(err, loanRequest) {
							console.log('Loan requests found'
									+ loanRequest.length);
							var borrowedAmount = 0, pendingAmount = 0, activeLoan = 0, paidAmount = 0, avgMonthlyEMI = 0, annualRate = 0;
							var loanRequestIds = [];
							var activeLoans = [];
							var activeLoanRequests = [];
							var pendingLoanRequests = [];

							for (var i = 0; i < loanRequest.length; i = i + 1) {
								
								pendingAmount = pendingAmount
										+ loanRequest[i].pendingAmount;
								if (loanRequest[i].status === 'active') {
									borrowedAmount = borrowedAmount	+ loanRequest[i].totalAmount;
									activeLoan = activeLoan + 1;
									avgMonthlyEMI = avgMonthlyEMI
											+ loanRequest[i].emiAmount;
									annualRate = annualRate
											+ loanRequest[i].interestRate;
									paidAmount = paidAmount + +(+loanRequest[i].emiAmount * +loanRequest[i].term- +loanRequest[i].pendingAmount).toFixed(2);
									var loan = {};
									loan = loanRequest[i];
									activeLoanRequests.push(loan);
								} else if (loanRequest[i].status === 'pending') {
									var loan = {};
									loan = loanRequest[i];
									var fund = 0;
									for (var k = 0; k < loanRequest[i].investments.length; k = k + 1) {
										fund = fund
												+ loanRequest[i].investments[k].amount;
									}
									loan['funds'] = fund;
									pendingLoanRequests.push(loan);
								}else if(loanRequest[i].status === 'completed'){
									borrowedAmount = borrowedAmount	+ loanRequest[i].totalAmount;
									paidAmount = paidAmount + +(+loanRequest[i].emiAmount * +loanRequest[i].term- +loanRequest[i].pendingAmount).toFixed(2);
								}
								loanRequestIds.push(loanRequest[i]._id);
							}

							responseBody['totalLoan'] = loanRequest.length;
							responseBody['activeLoan'] = activeLoan;
							responseBody['borrowedAmount'] = +borrowedAmount.toFixed(2);
							responseBody['paidAmount'] = +paidAmount.toFixed(2);
							responseBody['avgMonthlyEMI'] = +avgMonthlyEMI.toFixed(2);
							responseBody['avgAnnualRate'] = (activeLoan === 0) ? 0 : +(annualRate / activeLoan).toFixed(2);
							responseBody['activeLoanRequests'] = activeLoanRequests;
							responseBody['pendingLoanRequests'] = pendingLoanRequests;
							res.setHeader('Content-Type', 'application/json');
							res.send(responseBody);
						});
	},
	dashboardInvestor :  function(req, res) {
		var strUserId = req.query.userId;
		var userId = new BSON.ObjectID(strUserId);
		var amountInvested=0 , totalInvestments=0, activeInvestments=0;
		var responseBody={},gradeWiseAmountBucket={};
		console.log('In Investor Dashboard');
		db
				.collection("loanRequest")
				.find({
					'investments.investorId' : strUserId
				})
				.toArray(
						function(err, loanRequests) {
							for (var i = 0; i < loanRequests.length; i = i + 1) {
								if (loanRequests[i].investments != undefined) {
									totalInvestments = totalInvestments + 1;
									var status = loanRequests[i].status;
									if(status === 'active' || status === 'pending'){
										activeInvestments = activeInvestments + 1;
									}
									for (var k = 0; k < loanRequests[i].investments.length; k = k + 1) {
										{
											var investedAmount = +loanRequests[i].investments[k].amount;
											var grade = loanRequests[i].grade;
											amountInvested = amountInvested
													+ investedAmount;
											if (gradeWiseAmountBucket[grade] == undefined
													|| gradeWiseAmountBucket[grade] == null) {
												gradeWiseAmountBucket[grade] = investedAmount;
											} else {
												gradeWiseAmountBucket[grade] = gradeWiseAmountBucket[grade]
														+ investedAmount;
											}
										}
									}
								}
							}

							console.log('Investment amount is : '
									+ amountInvested);
							responseBody['amountInvested'] = amountInvested;
							responseBody['gradeWiseAmountBucket'] = gradeWiseAmountBucket;
							responseBody['totalInvestments'] = totalInvestments;
							responseBody['activeInvestments'] = activeInvestments;
							
							res.send(responseBody);
						});
	},
	getInvestmentsForInvestor : function(req, res) {
		var strUserId = req.query.userId;
		var responseBody={},gradeWiseAmountBucket={} , amountInvested = 0;
		console.log('In getInvestmentsForInvestor');
		db
				.collection("loanRequest")
				.find({
					'investments.investorId' : strUserId
				})
				.toArray(
						function(err, loanRequests) {
							for (var i = 0; i < loanRequests.length; i = i + 1) {
								if (loanRequests[i].investments != undefined) {
									for (var k = 0; k < loanRequests[i].investments.length; k = k + 1) {
										{
											var investedAmount = +loanRequests[i].investments[k].amount;
											var investedMode = loanRequests[i].investments[k].mode;
											var grade = loanRequests[i].grade;
											amountInvested = amountInvested
													+ investedAmount;
											if (gradeWiseAmountBucket[grade] == undefined
													|| gradeWiseAmountBucket[grade] == null) {
												gradeWiseAmountBucket[grade]={}; 
												gradeWiseAmountBucket[grade]['manual']=0;
												gradeWiseAmountBucket[grade]['auto']=0;
											}
											investedMode = (investedMode === 'auto'?investedMode:'manual');
											if (gradeWiseAmountBucket[grade][investedMode] == undefined
													|| gradeWiseAmountBucket[grade][investedMode] == null) {
												gradeWiseAmountBucket[grade][investedMode] = investedAmount;
											}else{	
											gradeWiseAmountBucket[grade][investedMode] = gradeWiseAmountBucket[grade][investedMode]
													+ investedAmount;
											}
										}
									}
								}
							}

							console.log('Investment amount is : '
									+ amountInvested);
							responseBody['amountInvested'] = amountInvested;
							responseBody['gradeWiseAmountBucket'] = gradeWiseAmountBucket;
							res.send(responseBody);
						});
	}
}