/**
 * New node file
 */
var LocalStrategy   = require('passport-local').Strategy;
var User = require('./user');


module.exports = function(passport){

	passport.use('local-signup', new LocalStrategy({
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {
        	var userAttributes=req.query;
    		userAttributes.email=username;
    		userAttributes.password=password;
    		
    		User.populateUser(userAttributes);
    		User.print();
    		User.registerUser(function(err,user){
    		
    		console.log('response of register request:'+JSON.stringify(user));
            // In case of any error, return using the done method
            if (err)
                return done(err);
            // Username does not exist, log the error and redirect back
            if (!user){
                console.log('User Not Found with username '+username);
                return done(null, false, req.flash('message', 'Could Not Authenticate User.'));                 
            }
            // User and password both match, return user from done method
            // which will be treated like success
            return done(null, user);
    		});

                    
        })
    );

    
}
