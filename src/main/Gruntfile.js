module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: false,
          yuicompress: false,
          optimization: 2
        },
        files: {
          "Application/app/assets/css/app.css": "Application/app/assets/css/less/app.less"
        }
      }
    },
    watch: {
      styles: {
        files: ['Application/app/assets/css/less/**/*.less'], // Watch all the less files(even in subdirectories)
        tasks: ['less'],
        options: {
          nospawn: true
        }
      }
    }
  });

  grunt.registerTask('default', ['less', 'watch']);
};