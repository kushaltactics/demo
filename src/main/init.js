var login = require('./login');
var signup = require('./signup');
var User = require('./user');

var jwt = require('json-web-token');
var secret = 'TOPSECRET';

module.exports = function(passport){

	/*
	These(serializeUser and deserializeUser) allow the user's data to be saved and retrieved from a session store. 
	This could be memory, memcached, or any other database.
	*/
	// Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(user, done) {

    	var user_id;
    	console.log("passport object"+JSON.stringify(passport));
    	console.log("passport object"+JSON.stringify(user));
    	console.log("passport object"+JSON.stringify(done));
    	
    	var sessionUser={
    		    "_id":user._id,
    			"firstName": user.firstName,
    		    "lastName": user.lastName,
    		    "middleName": user.middleName,
    		    "dateOfBirth": user.dateOfBirth,
    		    "email": user.email,
    		    "mobile":user.mobile,
    		    "homeAddress": user.homeAddress,
    		    "city": user.city,
    		    "state": user.state,
    		    "zip": user.zip,
    		    "password": user.password,
    		    "ssn": user.ssn,
    		    "creditScore": user.creditScore,
    		    "purpose":user.purpose,
    		    "amount":user.amount,
    		    "userType":user.userType,
    		    "term":user.term
    		}
    	console.log("sessionUser"+JSON.stringify(sessionUser));
    	// following stores the complete user object
    	//jwt.encode(secret,user, 'HS256', function (err, token) {
    	// this stores partial info of the user
		jwt.encode(secret,sessionUser, 'HS256', function (err, token) {
    	
		if (err) {
			return console.error(err.name, err.message);
		} else {
			user_id=token;
		}
		});
        
		done(null, user_id);
    });

    passport.deserializeUser(function(id, done) {
		
		jwt.decode(secret,id,function(err, decode) {
        if (err) {
            return console.error(err.name, err.message);
        } else {
            console.log("got decoded"+JSON.stringify(decode));
            done(null, decode);
        }
        
    });
	
    });

    // Setting up Passport Strategies for Login and SignUp/Registration
    login(passport);
    signup(passport);

}