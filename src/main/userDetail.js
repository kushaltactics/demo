var db = require('./dbConnectionModule.js');
var mongo = require('mongoskin');
var BSON = mongo.BSONPure;

function getSalary(){
	var randomnumber = Math.floor(Math.random() * (5)) + 1;
	var salaries = ["50,000", "70,000", "1,20,000", "35,000", "1,00,000", "1,15,000"];
	return salaries[randomnumber];
}

function getdti(){
	var randomnumber = Math.floor(Math.random() * (5)) + 1;
	var dti = ["50", "75", "62", "34", "47", "29"];
	return dti[randomnumber];
}

module.exports = {
	getUserDetails : function(req, res) {
		var userId = req.query.userId;
		var userObjectid = new BSON.ObjectID(userId);
		
		db.collection("users").find({
				'_id' : (userId !== undefined) ? userObjectid : null
			}).toArray(function(err, userObject) {

				if(userObject!= null && userObject.length > 0){
					res.setHeader('Content-Type', 'application/json');
   					res.send(JSON.stringify(userObject[0]));
				}
				else{
					res.send({"success" : false, "msg":"Invalid User Id"});
				}
		});
	},
	
	getEmploymentDetails : function(req, res) {
		var userId = req.query.userId;
		var userObjectid = new BSON.ObjectID(userId);
				
		db.collection("users").find({
				'_id' : (userId !== undefined) ? userObjectid : null
			},{employmentDetails:1}).toArray(function(err, userObject) {

				if(userObject!= null && userObject.length > 0){
					res.setHeader('Content-Type', 'application/json');
   					res.send(JSON.stringify(userObject[0]));
				}
				else{
					res.send({"success" : false, "msg":"Invalid User Id"});
				}
		});
	},
	
	getBankDetails : function(req, res) {
		var userId = req.query.userId;
		var userObjectid = new BSON.ObjectID(userId);
				
		db.collection("users").find({
				'_id' : (userId !== undefined) ? userObjectid : null
			},{bankDetails:1}).toArray(function(err, userObject) {

				if(userObject!= null && userObject.length > 0){
					res.setHeader('Content-Type', 'application/json');
   					res.send(JSON.stringify(userObject[0]));
				}
				else{
					res.send({"success" : false, "msg":"Invalid User Id"});
				}
		});
	},
	
	updateUserDetails : function(req, res) {
		var user = req.body;
		
		if(user == null)
		{
			res.send({"success" : false, "msg":"invalid user"});
		}
		var userId = new BSON.ObjectID(user._id);
		user._id = userId;
		
		db.collection("users").update(
				{ _id: userId },
				user,function(err, doc) {
				if (err) {
					console.log(JSON.stringify(err));
					res.send({"success" : false,"msg":"Unable to update user details"});
				} else {
						db.collection("users").find({
							'_id' : userId
						}).toArray(function(err, userObject) {
				
							if(userObject!= null && userObject.length > 0){
								res.send(userObject[0]);
							}
							else{
								res.send({});
							}
					});
				}	
		});
	},
	uploadProfilePic : function(req, res) {
		var userId = req.body.userId;
		var userObjectid = new BSON.ObjectID(userId);
		var uploadedImage = req.body.image;
		
		if(uploadedImage != null){
			db.collection("users").update(
					{ _id: userObjectid },
					{ $set:  {image : uploadedImage} },function(err, doc) {
					if (err) {
						console.log(JSON.stringify(err));
						res.send({"success" : false,"msg":"Unable to upload User Profile Picture."});
					} else {
							db.collection("users").find({
								'_id' : userObjectid
							}).toArray(function(err, userObject) {
					
								if(userObject!= null && userObject.length > 0){
									res.send(userObject[0]);
								}
								else{
									res.send({});
								}
						});
					}	
			});
		}
		else{
			res.send({"success" : false, "msg":"Invalid Image."});
		}
	},
	getBorrowerInfo : function(req, res) {
		var userId = req.query.userId;
		var userObjectid = new BSON.ObjectID(userId);
		
		db.collection("users").find({
				'_id' : (userId !== undefined) ? userObjectid : null
			}).toArray(function(err, userObject) {

				if(userObject!= null && userObject.length > 0){
					var user = userObject[0];
					var userInfo = "Borrower ";
					
					if(user.dateOfBirth){
						var dob = new Date(user.dateOfBirth);
						var currentDate = new Date();
						userInfo += (currentDate.getYear() - dob.getYear()) + " years old ";
					}
					if(user.maritalStatus){
						userInfo += ","+user.maritalStatus+" ";
					}
					if(user.city){
						userInfo += ", lives in ";
						userInfo += user.city+ " ";
					}
					if(user.state){
						if(!user.city){
							userInfo += ", lives in "+user.state+ " ";
						}
						else{
							userInfo += ","+user.state+ " ";
						}
					}
					if(user.employmentDetails && user.employmentDetails.jobTitle){
						userInfo += " and works as ";
						userInfo += user.employmentDetails.jobTitle+ " ";
					}
					if(user.employmentDetails && user.employmentDetails.employer){
						if(!(user.employmentDetails && user.employmentDetails.jobTitle)){
							userInfo += " and works at "+user.employmentDetails.employer+ " ";
						}
						else{
							userInfo += " at "+user.employmentDetails.employer+ " ";
						}
					}
					
					
					db.collection("loanRequest").count( { borrowerId: userObjectid, status: "active" }, function(err,count){
						if(err){
							console.log(JSON.stringify(err));
							res.send({"success" : false, "msg":"Some error occured."});	
						}
						else{
							userInfo +=  "with an annual income of $"+ getSalary() +" and Debt-to-Income ratio is "+ getdti() +"%. Active loans are "+count+".";
							var output = {"success" : true, "msg": userInfo , "gender": "m"};
							res.send(output);
						}
					});	
					
				}
				else{
					res.send({"success" : false, "msg":"Invalid User Id"});
				}
		});
	}
}
