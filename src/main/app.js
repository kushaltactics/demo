var express = require('express'),
bodyParser = require('body-parser'),
restCall=require('restler'),
session = require('express-session'),
cookieParser = require('cookie-parser'),
passport = require('passport'),
local = require('passport-local').Strategy,
TwitterStrategy = require('passport-twitter'),
GoogleStrategy = require('passport-google'),
FacebookStrategy = require('passport-facebook');
bcrypt = require('bcrypt-nodejs');

var app =express();
var logger=require('./utility/logger.js');
var constants=require('./config.js');
var loginModule=require('./utility/login.js');
var registerModule=require('./utility/register.js');
var softCreditModule=require('./utility/softCredit.js');
var loanRequestModule=require('./utility/loanRequest.js');
var userDetailModule=require('./utility/userDetail.js');
var dashboardModule=require('./utility/dashboard.js');
var investmentModule=require('./utility/investMoney.js');
var socialLoginModule=require('./utility/socialLogin.js');
var loanSummaryModule=require('./utility/loanSummary.js');

var MongoStore = require('connect-mongo')(session);

console.log("Express server running at => http://localhost:" + constants.port + "/\nCTRL + C to shutdown");
logger.log("debug","Application Started");

//app configuration
app.use( bodyParser.json() );       // to support JSON-encoded bodies

app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

//configure session with mongoDb as session store
//to run in production uncomment store to poin to mongoDb
app.use(session({
	secret: 'p2pDemo',
    resave: false,
    saveUninitialized: false
 /* ,store: new MongoStore({
      url: 'mongodb://localhost:27017/test',
});*/
  
}));


//Configuring Passport
app.use(passport.initialize());
// its another way of asking is user valid in session.
app.use(passport.session());

//Initialize Passport
var initPassport = require('./utility/init');
initPassport(passport);

//serves angular application
app.use(express.static("./Application"));

app.post('/register',registerModule.register);

app.post('/login', 
		  passport.authenticate('local'),
		  function(req, res) {
		    //res.redirect('/');
			console.log("passport call back called"+JSON.stringify(req.user));
			res.send(req.user);
		  });

app.get('/logout', function(req, res) {
	var name = req.user.email;
	console.log("LOGGIN OUT " + req.user.email)
	req.logout();
	res.redirect('/');
	req.session.notice = "You have successfully been logged out " + name
			+ "!";
});

app.get('/loggedin', function(req, res) {
	console.log("LoggedIn"+req.session.passport);
	res.send(req.isAuthenticated() ? req.user : '0');
});

//push dummy users to db for social login
app.get('/addSocialData',socialLoginModule.addData);
app.get('/socialLogin',socialLoginModule.socialLogin);

app.get('/getSoftCredit',softCreditModule.getSoftCredit);

app.get('/getUserDetails',userDetailModule.getUserDetails);

app.get('/getBorrowerInfo',userDetailModule.getBorrowerInfo);

app.get('/getEmploymentDetails',userDetailModule.getEmploymentDetails);

app.get('/getBankDetails',userDetailModule.getBankDetails);

app.get('/getEmi',loanRequestModule.getEmi);

app.get('/canCreateLoanRequest',loanRequestModule.canCreateLoanRequest)

app.get('/getRiskProfile',investmentModule.getRiskProfile);

app.post('/getRates',investmentModule.getRates);

app.get('/getLoanGrade',loanRequestModule.getLoanGrade);

app.get('/getLoanRequestList',loanRequestModule.getLoanRequestList);

app.get('/payEmi',loanRequestModule.payEmi);

app.get('/dashboardBorrower',dashboardModule.dashboardBorrower);

app.get('/dashboardInvestor',dashboardModule.dashboardInvestor);

app.get('/getInvestmentsForInvestor',dashboardModule.getInvestmentsForInvestor);

app.post('/loanRequest',loanRequestModule.requestLoan);

app.post('/investMoney',investmentModule.investMoney);

app.post('/autoInvest', investmentModule.autoInvest);

app.post('/updateUserDetails', userDetailModule.updateUserDetails);

app.post('/uploadProfilePic', userDetailModule.uploadProfilePic);

app.get('/getLoanSummary', loanSummaryModule.getLoanSummary);

app.get('/captcha', function(req, res){

    var captcha = require('node-svgcaptcha');
    var options = {length: 6, width: 150 };//Set your configuration in this object
    var genCaptcha = captcha(options);
    
    if(req.session){//save value in session
        req.session.captcha = genCaptcha.captchaValue;        
    }
    
    //return svg to render in the browser
    res.set('Content-Type', 'image/svg+xml');
    res.send(genCaptcha.svg);   
});




app.use('*', function(req, res, next) {
	  var err = new Error();
	  err.status = 404;
	  next(err);
	});

	app.use(function(err, req, res, next) {
	  if(err.status == 404) {
	      console.error("PAGE NOT FOUND:- "+req.url);
	  }else{
	  console.error("ERROR OCCURRED- "+"STATUS:- "+err.status+" Reason:-"+err.stack);
	  }
	  //return next();
	  res.status(500).json({ error: 'Some Error Occurred' });
	  //res.send('Some Error Occurred');
	});
app.listen(constants.port);