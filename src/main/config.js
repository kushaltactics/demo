// config.js/
module.exports = {
	
		//express port number
		port:8082,
		//DB Configuration
		dbUrl:'mongodb://localhost:27017/test',
		
		//smtp server details
		smtpServer:'172.21.5.10',
		smtpPort:25,
		
		//social login
		fbUrl:'https://graph.facebook.com/oauth/authorize',
		clientId:'414144168755690',
		redirectUri:'https://playwithedge.com/storeFront/',
		authUrl:'https://graph.facebook.com/oauth/access_token',
		clientSecret:'46d2453ec11bd9b33a987ceaac4c6d4e',
		meUrl:'https://graph.facebook.com/me/',
	    
		environment: 'DEV'

}