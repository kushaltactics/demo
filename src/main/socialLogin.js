var db = require('./dbConnectionModule.js');
var mongo = require('mongoskin');
var BSON = mongo.BSONPure;
var bcount = 1;
var icount = 1;

module.exports = {
	socialLogin : function(req, res) {
		
		var userType=req.query.userType;
		console.log("social Login userType"+userType);
		db.collection("sociallogin").find({
			'userType' : (userType !== undefined) ? userType : null
		}).toArray(function(err, userObject) {

			console.log("get User to Array"+JSON.stringify(userObject));
			if(userObject!= null && userObject.length > 0){
				var index = userObject[0].email.indexOf("@");
				if(userType === 'borrower'){
					var updatedEmail = [userObject[0].email.slice(0, index), bcount, userObject[0].email.slice(index)].join('');
					userObject[0].email = updatedEmail;
					bcount++;
				}
				else{
					var updatedEmail = [userObject[0].email.slice(0, index), icount, userObject[0].email.slice(index)].join('');
					userObject[0].email = updatedEmail;
					icount++;
				}
				//Default fields initialization
				userObject[0]['image']="";
				userObject[0]['mortgageDetails']={};
				userObject[0]['employmentDetails']={};
				userObject[0]['bankDetails']={};
				userObject[0]['wallet']={};
				userObject[0]['portfolio']={};
				res.send(userObject[0]);
			}
			else{
				res.send("not available");
			}
		});//end of toArray	
	},
	
	addData : function(req,res){
		 
		//reads data from file pushes to mongoDb
		res.send("data Created");
	}
};