app.controller('registerController', ['$scope', '$rootScope', '$location', 'registerService', 'mainService', '$mdToast', '$sessionStorage', '$translate', 'landingService', '$routeParams', '$mdDialog',
    function ($scope, $rootScope, $location, registerService, mainService, $mdToast, $sessionStorage, $translate, landingService, $routeParams, $mdDialog) {
		// Constants from rootScope
		var uiConstants = $rootScope.data.uiConstants;

		$rootScope.data.user.referralDetails = { referredCode: $routeParams.referredCode };
		$scope.countries = [];
		$scope.userRegistered = true;

		$scope.viewLogin = true;
		$scope.model = {
			password: ""
		};

		if (angular.isDefined($rootScope.data.user.mobile) && $rootScope.data.user.mobile != null) {
			$scope.viewLogin = false;
		} else {
			$rootScope.data.user.mobile = null;
		}

		$scope.showVerifyMobile = function ($event) {
			// Check that country is selected
			var countryShortName = $rootScope.data.user.address.countryName;
			if (!angular.isDefined(countryShortName) || countryShortName.trim().length === 0) {
				$translate('register.ERR_SELECT_COUNTRY').then(function (translation) {
					$mdToast.show(
						$mdToast.simple()
							.content(translation)
							.hideDelay(uiConstants.TOAST_SHOW_TIME_MS)
						);
				});
				return;
			}

			if (!angular.isDefined($rootScope.data.user.mobile) || $rootScope.data.user.mobile === null || $rootScope.data.user.mobile.length !== 10) {
				$translate('register.ERR_MOBILE_NO_LENGTH', { noOfDigits: 10 }).then(function (translation) {
					$mdToast.show(
						$mdToast.simple()
							.content(translation)
							.hideDelay(uiConstants.TOAST_SHOW_TIME_MS)
						);
				});
				return;
			}

			$scope.userRegistered = false;

			$mdDialog.show({
				targetEvent: $event,
				clickOutsideToClose: true,
				scope: $scope,
				preserveScope: true,
				templateUrl: '../../app/mobile_partials/templates/loginTypeTemplate.html',
				locals: {
					scope: $scope
				},
				controller: loginTypeController
			});
		};

		$scope.showLogin = function () {
			// Check that country is selected
			var countryShortName = $rootScope.data.user.address.countryName;
			if (!angular.isDefined(countryShortName) || countryShortName.trim().length === 0) {
				$translate('register.ERR_SELECT_COUNTRY').then(function (translation) {
					$mdToast.show(
						$mdToast.simple()
							.content(translation)
							.hideDelay(uiConstants.TOAST_SHOW_TIME_MS)
						);
				});
				return;
			}
			$scope.viewLogin = !$scope.viewLogin;
		};
		
		// Fetch the list of countries
		landingService.getCountries().then(function (response) {
			jQuery.each(response.data, function (i, n) {
				if (i !== undefined && i !== null && i !== "" && n.name !== undefined && n.name !== null && n.name !== "") {
					var country = {
						"shortName": i,
						"longName": n.name,
						"dialCode": n.dial_code
					};
					$scope.countries.push(country);
				}
			});
		});

		$scope.changeCountryCode = function () {
			var code = $rootScope.data.user.address.countryName;
			if (angular.isDefined(code) && code.trim().length !== 0) {
				landingService.getCountry(code.toUpperCase()).then(function (response) {
					var country = response.data[code.toUpperCase()];
					if (angular.isDefined(country)) {
						$rootScope.data.user.address.countryISD = country.dial_code;
					}
				});
			} else {
				$rootScope.data.user.address.countryISD = "";
			}
		};

		$scope.loginUser = function ($event) {
			// Check that country is selected
			var countryShortName = $rootScope.data.user.address.countryName;
			if (!angular.isDefined(countryShortName) || countryShortName.trim().length === 0) {
				$translate('register.ERR_SELECT_COUNTRY').then(function (translation) {
					$mdToast.show(
						$mdToast.simple()
							.content(translation)
							.hideDelay(uiConstants.TOAST_SHOW_TIME_MS)
						);
				});
				return;
			}

			$scope.userRegistered = true;

			$mdDialog.show({
				targetEvent: $event,
				clickOutsideToClose: true,
				scope: $scope,
				preserveScope: true,
				templateUrl: '../../app/mobile_partials/templates/loginTypeTemplate.html',
				locals: {
					scope: $scope
				},
				controller: loginTypeController
			});
		};

		var loginTypeController = ['$route', '$scope', '$rootScope', '$mdDialog', 'scope', 'mainService', '$location', '$mdToast', '$translate',
			function ($route, $scope, $rootScope, $mdDialog, scope, mainService, $location, $mdToast, $translate) {
				$scope = scope;
				$scope.selectUserType = function (userType) {
					$rootScope.data.user.userType = userType;

					if (scope.userRegistered) {
						var data = {
							mobile: $rootScope.data.user.mobile,
							password: $scope.model.password,
							userType: $rootScope.data.user.userType
						};
						mainService.login(data).then(function (response) {
							var userUrl = 'getUserDetails?userId=' + response.data._id;
							mainService.get(userUrl).then(function (res) {
								$rootScope.data.user = res.data;
								$rootScope.isLoggedIn = true;
								$rootScope.data.user.age = moment().diff($rootScope.data.user.dateOfBirth, 'years');

								// Broadcast the availability of user in rootscope
								$rootScope.$broadcast('userAvailable', $rootScope.data.user);
								
								// Broadcast for fetching notifications
								$rootScope.$broadcast('getNotifications');

								if ($rootScope.data.user.userType === "borrower") {
									if ($rootScope.data.user.dataProvided.personalDetails)
										$location.path("dashboard");
									else
										$location.path("enterProfile");
								} else if ($rootScope.data.user.userType === "investor") {
									$location.path("dashboardInvestor");
								}
							});
						}, function (response) {
							$translate('common.ERR_INCORRECT_LOGIN').then(function (translation) {
								$mdToast.show($mdToast.simple().content(translation).hideDelay(uiConstants.TOAST_SHOW_TIME_MS));
							});
						});
					}
					else {
						$mdDialog.hide();
						$location.path("verifyMobile")
					}
					$mdDialog.hide();
				}
			}];

		$scope.selectedCountry = {
			"shortName": $rootScope.data.user.address.countryName,
			"longName": $rootScope.data.user.address.countryLongName,
			"dialCode": $rootScope.data.user.address.countryISD
		};

		$scope.doCountrySearch = function (query) {
			var results = query ? $scope.countries.filter($scope.filterCountrySearch(query)) : $scope.countries;
			return results;
		};

		$scope.countryTextChange = function (text) {
			// Will be fired when the text is changed
		};

		$scope.countrySelectedItemChange = function (item) {
			// Will be fired when item is selected from the list
			if (angular.isDefined(item)) {
				$rootScope.data.user.address.countryName = item.shortName;
				var code = item.shortName;
				if (angular.isDefined(code) && code.trim().length !== 0) {
					landingService.getCountry(code.toUpperCase()).then(function (response) {
						var country = response.data[code.toUpperCase()];
						if (angular.isDefined(country)) {
							$rootScope.data.user.address.countryISD = country.dial_code;
						}
					});
				} else {
					$rootScope.data.user.address.countryISD = "";
				}
			}
		};

		$scope.filterCountrySearch = function (query) {
			var lowercaseQuery = angular.lowercase(query);
			return function filterFn(country) {
				return (angular.lowercase(country.longName).indexOf(lowercaseQuery) === 0);
			};
		};
    }]);