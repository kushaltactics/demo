/**
 * New node file
 */
var email = require('./email.js');
var db = require('./dbConnectionModule.js');
module.exports = {
	register : function(req, res) {
		var registrationData = req.body;
		// Validations
		var valid = false;
		console.log("registration"+JSON.stringify(registrationData));
		
		if (registrationData.firstName != null
				&& registrationData.lastName != null
				&& registrationData.email != null
				&& registrationData.password != null) {
			valid = true;
		} else {
			res.send({"success":false,"msg":"Mandatory fields not available."});
		}
		
		if(req.session && (req.session.captcha !== registrationData.captcha)){
			valid = false;
			res.send({"success":false,"msg":"Captcha does not match."});
	    }
		
		//Email validation
		if (valid) {
			db.collection("users").find({
				'email' : registrationData.email}).toArray(function(err, userObject) {
				if(userObject!= null && userObject.length > 0){
					valid = false;
					res.send({"success":false,"msg":"Email already registered."});
				}
				else{
					valid = true;
				}
				
				if (valid) {
					//encrypt password
					registrationData.password = bcrypt.hashSync(registrationData.password);
					
					db.collection("users").insert(registrationData, function(err, doc) {
						if (err) {
							// Send error message.
							res.send("Technical Difficulty.Try some other day !!");
						} else {
							// And forward to success page
							mail = {"toAddress":registrationData.email, "subject":"Registration Successful", "body":"<b>Thanks for registering at ZEVA."};
							email.sendMail(mail);	
							res.send({"success":true,"msg":"User Registered Successfully."});
						}
					});
				} 
			});
		}
	}
}
