var serviceUrls = {
	COMMON: "http://localhost:8089/"
};

var errorConstants = {
	SERVER_UNREACHABLE: "Unable to reach the Zeva Server.",
	RETRY_TXT_START: "Retrying in ",
	RETRY_TXT_END: " seconds.",
	RETRY_NOW: "Retry now."
};