nodemailer = require('nodemailer');
smtpTransport = require('nodemailer-smtp-transport');
constants=require('../config.js');

transporter = nodemailer.createTransport(smtpTransport({
							    host: constants.smtpServer,
							    port: constants.smtpPort,
							    authMethod : 'PLAIN',
							    secure: false,
							    ignoreTLS:true,
                         		tls: {rejectUnauthorized: false}	
}));

module.exports = {
	sendMail : function(mail) {
	
		var mailOptions = {
		    from: 'P2P_AutoMailer@edgeverve.com', // sender address
		    to: mail.toAddress, // list of receivers eg. ankit_singh16@edgeverve.com, Satyendra_Naruka@edgeverve.com, Chandan_Das02@edgeverve.com, Tejas_Patil01@edgeverve.com, Kushal_Jain@edgeverve.com
		    subject: mail.subject, // Subject line eg. 'Hello P2P Developers'
		    text: mail.body, // plaintext body eg.'text'
		    html: mail.body  // html body eg. '<b>Chithhi aayi hai.. aayi hai ..chitthi aayi hai</b>'
		};
		
		// send mail with defined transport object
		transporter.sendMail(mailOptions, function(error, info){
		    if(error){
		        return console.log(error);
		    }
		    console.log('Message sent: ' + info.response);
		
		});
	}		
}
