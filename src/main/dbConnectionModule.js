var mongo = require('mongoskin');
var constants=require('../config.js');
var db = mongo.db(constants.dbUrl, {
	native_parser : true
});
module.exports=db;