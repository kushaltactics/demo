/**
 * It is a model that represents user from ecomm system.
 */


var db=require('./dbConnectionModule.js');
function User () {
		this._id,
		this.firstName;
		this.lastName;
	    this.middleName;
	    this.dateOfBirth;
	    this.email;
	    this.mobile;
	    this.homeAddress;
	    this.city;
	    this.state;
	    this.zip;
	    this.password;
	    this.ssn;
	    this.userCreditRating


}

User.prototype.print = function(){
	console.log("Print"+JSON.stringify(this));
}

User.prototype.populateUser = function(data){
	
	if(data.hasOwnProperty("_id")){
		this._id=data._id;
	}
	if(data.hasOwnProperty("firstname")){
		this.firstname=data.firstname;
	}
	if(data.hasOwnProperty("lastName")){
		this.lastName=data.lastName;
	}
	
	if(data.hasOwnProperty("email")){
		this.email=data.email;
	}
	if(data.hasOwnProperty("middleName")){
		this.middleName=data.middleName;
	}
	if(data.hasOwnProperty("dateOfBirth")){
		this.dateOfBirth=data.dateOfBirth;
	}
	if(data.hasOwnProperty("homeAddress")){
		this.homeAddress=data.homeAddress;
	}
	if(data.hasOwnProperty("city")){
		this.city=data.city;
	}
	if(data.hasOwnProperty("state")){
		this.state=data.state;
	}
	if(data.hasOwnProperty("zip")){
		this.zip=data.zip;
	}
	if(data.hasOwnProperty("password")){
		this.password=data.password;
	}
	if(data.hasOwnProperty("ssn")){
		this.ssn=data.ssn;
	}
	if(data.hasOwnProperty("creditScore")){
		this.creditScore=data.creditScore;
	}
	if(data.hasOwnProperty("mobile")){
		this.mobile=data.mobile;
	}
	if(data.hasOwnProperty("userType")){
		this.userType=data.userType;
	}
	
}

 
User.prototype.getUser = function(calBack) {
	var result;
	console.log("Get User"+JSON.stringify(this));
	
	//find user is in the DB and respond
	
	
	var sessionId = null;
	var email=this.email;
	var password=this.password;
	
	db.collection("users").find({
		'email' : (email !== undefined) ? email : null
	}).toArray(function(err, userObject) {

		console.log("get User to Array"+JSON.stringify(userObject));
		if(userObject!= null && userObject.length > 0){
			//sessionId = uuid.v1();
			this._id=userObject[0]._id;
			userObject[0]._id=userObject[0]._id;
			calBack(null,userObject[0]);
		}
		else{
			calBack(null,false);
		}
		

	});//end of toArray
		
};


User.prototype.registerUser = function(calBack) {


	var registrationData = this;
	// Validations
	var valid = true;
	if (registrationData.firstName != null
			&& registrationData.lastName != null
			&& registrationData.dob != null
			&& registrationData.email != null
			&& registrationData.zip != null
			&& registrationData.password != null) {
		valid = true;
	} else {
		res.send("Mandatory fields are empty.");
		valid = false;
	}
	
	//Email validation
	db.collection("users").find({
		'email' : registrationData.email}).toArray(function(err, userObject) {
		if(userObject!= null && userObject.length > 0){
			res.send("Email already registered.");
			valid = false;
		}
		else{
			valid = true;
		}
		
		if (valid) {
			db.collection("users").insert(registrationData, function(err, doc) {
				if (err) {
					// Send error message.
					res.send("Technical Difficulty.Try some other day !!");
				} else {
					// And forward to success page
					res.send("Registered Successfully.");
				}
			});
		} 
	});

	


};


module.exports = new User();