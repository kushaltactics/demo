•	Clone the project “P2P-Zeva-Cordova”
•	Install cordova cli
•	npm install -g cordova
•	Copy the extracted “dist\zeva-mobile-client.zip” to “www” folder of above project
•	Set environment variables for android:
•	set ANDROID_HOME=path/to/android/sdk/base/folder
•	set JAVA_HOME=path/to/jdk/base/folder
•	set path=%path%;%ANDROID_HOME%/tools;%ANDROID_HOME%/platform_tools
•	From the root folder(i.e. P2P-Zeva-Cordova\zeva) open command prompt and run the command
•	cordova build android (If you are running this for first time make sure you do that on open internet and not through proxy so that 1 time download of important libraries can be done. For open internet you can use mobiles internet or can do from home)
•	This will generate apk in “platforms\android\build\outputs\apk\android-debug.apk”. Deploy the apk manually or use below command:
•	adb install platforms\android\build\outputs\apk\android-debug.apk (This requires your device and computer should be in same network. See next step for doing this.)

For debugging see the attached file
