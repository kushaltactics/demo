app.factory('registerService', ['mainService',
    function (mainService) {
        return {
            register: function (json) {
                return mainService.post("m/register", json)
                    .then(function (result) {
                        return result.data;
                    });
            }
        };
    }]);